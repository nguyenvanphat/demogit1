let listProduct = [];
const getListProduct = function () {
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products ",
    method: "GET",
  })
    .then((res) => {
      listProduct = res.data;
      renderProductList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
getListProduct();
let renderProductList = function (arr) {
  let htmlContent = "";
  for (let item of arr) {
    htmlContent += `
        <tr>
        <td>
          <img
            style="width: 50px"
            src="${item.image}"
            alt=""
          />
        </td>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.description}</td>
        <td>${item.price}</td>
        <td>${item.inventory}</td>
        <td>${item.rating}</td>
        <td>${item.type}</td>
        <td>
          <button class="btn btn-danger" onclick="deleteProduct(${item.id})">
            <i class="fa fa-trash"></i>
          </button>
        </td>
        <td>
          <button class="btn btn-primary" onclick="showProductInfo(${item.id})">
            <i class="fa fa-pencil"></i>
          </button>
        </td>
      </tr>
        `;
  }
  document.getElementById("contentProduct").innerHTML = htmlContent;
};
let addNewProduct = function () {
  let image = document.getElementById("image").value;
  let name = document.getElementById("nameProduct").value;
  let id = document.getElementById("idProduct").value;
  let price = document.getElementById("priceProduct").value;
  let decription = document.getElementById("Decription").value;
  let amout = document.getElementById("amoutProduct").value;
  let rating = document.getElementById("rating").value;
  let typeProduct = document.getElementById("typeProduct").value;

  var isValid = true;
  isValid &=
    checkRequired(image, "imageErr") &&
    checkRequired(name, "idErr") &&
    checkRequired(id, "nameErr") &&
    checkRequired(price, "DecriptionErr") &&
    checkRequired(decription, "priceProductErr") &&
    checkRequired(amout, "amoutErr") &&
    checkRequired(rating, "ratingErr") &&
    checkRequired(typeProduct, "typeErr");

  if (!isValid) {
    return;
  }

  if (findByID(id) !== -1) {
    alert("mã sản phẩm ai cho nhập trùng ?? ");
    return;
  }

  let pro = new product(
    id,
    name,
    image,
    decription,
    price,
    amout,
    rating,
    typeProduct
  );

  axios({
    url: " https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    method: "POST",
    data: pro,
  })
    .then((res) => {
      alert("thêm sản phẩm thành công !!");
      getListProduct();
    })
    .catch((err) => {
      console.log(err);
    });
};

const findByName = function () {
  let result = [];
  let input = document.getElementById("idInput").value.trim().toLowerCase();
  for (let item of listProduct) {
    if (item.id === input) {
      result.push(item);
      break;
    }
  }
  for (let item of listProduct) {
    let name = item.name.toLowerCase();
    if (name.indexOf(input) !== -1) {
      result.push(item);
    }
  }
  renderProductList(result);
};
const deleteProduct = function (id) {
  axios({
    url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      alert("xóa sản phẩm thành công ");
      getListProduct();
    })
    .catch((err) => {
      console.log(err);
    });
};

let showProductInfo = function (id) {
  document.getElementById("addNewProduct").style.display = "none";
  document.getElementById("idUpdate").style.display = "block";
  document.getElementById("cancelUpdate").style.display = "block";
  axios({
    url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
    method: "GET",
  })
    .then((res) => {
      document.getElementById("image").value = res.data.image;
      document.getElementById("nameProduct").value = res.data.name;
      document.getElementById("idProduct").value = res.data.id;
      document.getElementById("idProduct").disabled = true;
      document.getElementById("priceProduct").value = res.data.price;
      document.getElementById("Decription").value = res.data.description;
      document.getElementById("amoutProduct").value = res.data.inventory;
      document.getElementById("rating").value = res.data.rating;
      document.getElementById("typeProduct").value = res.data.type;
    })
    .catch((err) => {
      console.log(err);
    });
};
let updateProduct = function () {
  let image = document.getElementById("image").value;
  let name = document.getElementById("nameProduct").value;
  let id = document.getElementById("idProduct").value;
  document.getElementById("idProduct").disabled = false;
  let price = document.getElementById("priceProduct").value;
  let decription = document.getElementById("Decription").value;
  let amout = document.getElementById("amoutProduct").value;
  let rating = document.getElementById("rating").value;
  let typeProduct = document.getElementById("typeProduct").value;

  let pro = new product(
    id,
    name,
    image,
    decription,
    price,
    amout,
    rating,
    typeProduct
  );

  axios({
    url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
    method: "PUT",
    data: pro,
  })
    .then((res) => {
      getListProduct();
      alert("cập nhật danh sách sản phẩm thành công ");
    })
    .catch((err) => {
      console.log(err);
    });
  cancelUpdate();
};
let cancelUpdate = function () {
  document.getElementById("image").value = "";
  document.getElementById("nameProduct").value = "";
  document.getElementById("idProduct").value = "";
  document.getElementById("idProduct").disabled = false;
  document.getElementById("priceProduct").value = "";
  document.getElementById("Decription").value = "";
  document.getElementById("amoutProduct").value = "";
  document.getElementById("rating").value = "";
  document.getElementById("typeProduct").value = "";

  document.getElementById("addNewProduct").style.display = "block";
  document.getElementById("idUpdate").style.display = "none";
  document.getElementById("cancelUpdate").style.display = "none";
};
let findByID = function (id) {
  for (let index in listProduct) {
    if (listProduct[index].id == id) {
      return index;
    }
  }
  return -1;
};
function checkRequired(value, errorId) {
  if (value) {
    document.getElementById(errorId).innerHTML = "";
    return true;
  }
  document.getElementById(errorId).innerHTML = "*trường này bắt buộc nhập";
  return false;
}
